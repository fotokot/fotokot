<?php
// сообщение, которое вы хотите подписать для того, чтобы получатели могли
// проверить, что его послали именно вы
$data = <<<EOD

Разрешаю потратить на обед с контрагентом не более 100,000 рублей.

Ваш директор.
EOD;
// сохраняем сообщение в фалй
$fp = fopen("msg.txt", "w");
fwrite($fp, $data);
fclose($fp);
// шифруем
if (openssl_pkcs7_sign("msg.txt", "signed.txt", "file://mycert.pem",
    array("file://mycert.pem", "mypassphrase"),
    array("To" => "joes@example.com", // ассоциативный синтаксис
          "From: HQ <ceo@example.com>", // индексированный синтаксис
          "Subject" => "Представительские расходы")
    )) {
    // message signed - send it!
    exec(ini_get("sendmail_path") . " < signed.txt");
}
?>