<?php
/* Открыть соединение */
$mysqli = new mysqli("localhost", "root", "P@ssw0rd", "ufk");

/* Проверить соединение */
if (mysqli_connect_errno()) {
    printf("Подключение не удалось: %s\n", mysqli_connect_error());
    exit();
}

/* Вывод текущей кодировки */
//$charset = $mysqli->character_set_name();
//printf ("Текущая кодировка - %s\n", $charset);

$mysqli->close();
?>
